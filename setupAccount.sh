#!/bin/bash

die () {
    echo >&2 "$@"
    exit 1
}

[ $# -eq 1 ] || die "1 argument required, $# given"

userName="$1"

sudo useradd --create-home --shell /bin/bash "$userName"
sudo usermod -aG sudo "$userName"
sudo passwd "$userName"
sudo apt-get update
sudo apt-get --assume-yes install openssh-server
ip -4 -o address list
