### TODO (kopierat från uppgiften)

Här är någon struktur, tänker vi kan checka av när vi gjort något och hojta i discord.
En struktur är att bygga klasser -> test -> flask-app -> frontend??

## Die-metod 
+ <del>Attribut https://dbwebb.se/uppgift/yahtzee1-v2#die-attr<del>
+ <del>Metoder https://dbwebb.se/uppgift/yahtzee1-v2#die-met<del>


## Hand-metoder
+ <del>Attribut https://dbwebb.se/uppgift/yahtzee1-v2#hand-attr</del>
+ <del>Metoder https://dbwebb.se/uppgift/yahtzee1-v2#hand-met</del>


## Krav 
+ <del>Filstruktur</del>
+ <del>Bilder under static/img</del>
+ <del>Spara kod i me/kmom02/yathzee1</del>
+ <del>Implementera koden från klassdiagrammen</del>
+ <del>Spara klassdiagramkod i me/kmom02/yahtzee1/src</del>
+ <del>Skriv tester för Die klassen</del>
+ <del>Test: Att skapa ett objekt utan skicka argument till konstruktorn.</del>
+ <del>Test: Att skapa ett objekt och skicka värde på tärningen till konstruktorn.</del>
+ <del>Test: Att skapa ett objekt och skicka ett otillåtet värde på tärningen, som till exempel 100, till konstruktorn</del>
+ <del>Test: Att roll() slumpar nytt värde.</del>
+ <del>Test: Att get_name() returnerar korrekt namn.</del>
+ <del>Test: Att __str__() returnerar rätt värde som en sträng.</del>
+ <del>Test: använd random.seed() när ni testar funktionalitet som slumpar värden</del>
+ <del>Startfilen ska heta app.py och vara körbar via app.cgi. app.cgi behöver bara fungera på studentservern.</del>
+ <del>Du har installerat modulerna Flask i en virtuell miljö.</del>
+ <del >Applikationen ska använda Bootstrap.</del>
+ <del>Applikationen ska ha routen main. </del>
+ <del>Applikationen: I main routen ska ni skapa ett Hand objekt med fem slumpade tärningar. </del>
+ <del>Frontend: På routens sida ska ni visa värdet på handens fem tärningar med hjälp av bilderna som finns i exampel/yahtzee/img. Det finns en bild för varje tärningsslag mellan 1 och 6, med namnen one.png, two.png, three.png, four.png, five.png och six.png. T.ex. om handen slog tärningarna 1, 4, 2, 4, 4 så ska ni visa bilderna one.png, four.png, two.png, four.png och four.png.</del>
+ <del>Ni ska också ha routen about där ni på sidan skriva era namn och akronymer.</del>
+ <del>header.html och footer.html ska inkluderas med Jinja2.</del>
+ <del>Er navbar ska innehålla ett namn på ert Yahtzee spel och länkar till båda routes.</del>
+ Testa, validera och publicera applikationen på studentservern.


### Bra att ha

Yahtzee regler: https://gamerules.com/rules/yahtzee-dice-game/

## Tänk på när du gör testfall
+ Minst en testklass per klass
+ Testerna ska gå att upprepa
+ Testerna ska vara oberoende
+ Ingen I/O i testkoden
+ Både positiva och negativa testfall
+ Testa all funktionalitet

## Att skriva testfall
+ Arrange, förbered inför testfallet
+ Act, gör ditt test
+ Assert, validera utfallet av ditt test