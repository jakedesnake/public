#!/bin/bash

setxkbmap se

writeVariables () {
    read -e -p "Enter git username: " gitUsername
    echo "gitUsername=$gitUsername" >> ~/myValues.config
    echo " "
    
    read -e -p "Enter public git repo name: " -i "public" gitPublicRepo    
    echo "gitPublicRepo=$gitPublicRepo" >> ~/myValues.config
    echo " "
    
    read -e -p "Enter private git repo name: " -i "private" gitPrivateRepo    
    echo "gitPrivateRepo=$gitPrivateRepo" >> ~/myValues.config
    echo " "
   
    read -e -s -p "Enter git password: " gitPassword
    echo "gitPassword=$gitPassword" >> ~/myValues.config
    echo " "
}

writeVariables

sudo apt-add-repository universe
sudo apt-get update
sudo apt-get --assume-yes install git

git clone https://"$gitUsername"@bitbucket.org/"$gitUsername"/"$gitPublicRepo".git

cp ~/.bashrc ~/.bashrc_$( date '+%H_%M_%S' )
cat ~/"$gitPublicRepo"/.bashrc >> ~/.bashrc
# printf "source ~/"$gitPublicRepo"/myFunctions.lib \n" >> ~/.bashrc
printf 'source ~/%q/myFunctions.lib\n' "$gitPublicRepo" >> ~/.bashrc
