#!/bin/bash

setxkbmap se

getGitValues() {
    echo " "
    echo "enter git username"
    echo " "
    read gitUsername
    echo "gitUsername=$gitUsername" >> ~/myValues.config

    echo " "
    echo "enter git repo"
    echo " "
    read gitReponame
    echo "gitReponame=$gitReponame" >> ~/myValues.config

    echo " "
    echo "enter git password"
    echo " "
    read -s gitPassword
    echo "gitPassword=$gitPassword" >> ~/myValues.config
}

sudo apt-add-repository universe
sudo apt-get update
sudo apt-get --assume-yes install git
git config --global user.email "liveuser@ubuntu"
git config --global user.name "jake"
# git clone https://"$gitUsername"@bitbucket.org/"$gitUsername"/public.git
